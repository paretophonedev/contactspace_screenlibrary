var sys_regex_number_pattern                = /\d+/g;
var sys_regex_currency_pattern              = /^\d*.?\d*$/;
var sys_payment_option_value_cc             = 'Credit Card';
var sys_payment_option_value_cce            = 'Credit Card Existing';
var sys_payment_option_value_dd             = 'Direct Debit';
var sys_payment_option_value_dde            = 'Direct Debit Existing';
var sys_payment_option_value_sf             = 'Send Form';
var sys_validation_colour_fail              = 'indianred';
var sys_validation_colour_pass              = '#0ad80a';
var sys_style_col_background                = '#d7d7d7';
var sys_style_col_navtext                   = 'white';
var sys_validated_pass_keyword              = '1';
var sys_validated_fail_keyword              = '0';

var trigger_validate_dd                     = 'trigger_validate_dd';
var trigger_using_variable_display_seg      = 'trigger_using_variable_display';
var trigger_using_variable_display_freq     = 'trigger_using_variable_display_freq';
var trigger_using_variable_display_dec      = 'trigger_using_variable_display_dec';

var custom_scripting_no_4w2m = 'custom_scripting_no_4w2m';

var keyword_auto_update                     = 'span.auto_update_';
var keyword_variable_display                = 'marker_variable_display';
var keyword_variable_display_default        = 'marker_variable_display_default';

var option_ppd_required_on_new              = 'option_ppd_required_on_new';
var option_address_required                 = 'option_address_required';
var option_payment_option_style             = 'option_payment_option_style';
var option_deferral_months                  = 'option_deferral_months';
var option_min_rg                           = 'option_min_rg';
var option_min_cash                         = 'option_min_cash';
var option_min_arrears                      = 'option_min_arrears';
var option_payment_frequencies_available    = 'option_payment_frequencies_available';
var option_payment_frequencie_override      = 'option_payment_frequencie_override';
var option_payment_dates_available          = 'option_payment_dates_available';
var option_payment_frequency_changeable     = 'option_payment_frequency_changeable';
var option_payment_dates_changeable         = 'option_payment_dates_changeable';
var option_deferral_changeable              = 'option_deferral_changeable';
var option_ps_instance                      = 'option_ps_instance';
var option_mw_instance                      = 'option_mw_instance';
var option_arrears_changeable               = 'option_arrears_changeable';
var option_can_arrears_only                 = 'option_can_arrears_only';
var option_dynamic_payment_dates            = 'option_dynamic_payment_dates';
var option_dynamic_payment_dates_Month      = 'option_dynamic_payment_dates_Month';
var option_dynamic_payment_dates_FourWK     = 'option_dynamic_payment_dates_FourWK';
var option_dial_mode                        = 'option_dial_mode';


var option_arrears_any  = 'option_arrears_any';
var option_arrears_cc   = 'option_arrears_cc';
var option_arrears_cce  = 'option_arrears_cce';
var option_arrears_dd   = 'option_arrears_dd';
var option_arrears_dde  = 'option_arrears_dde';
var option_can_send_form = 'option_can_send_form';

var field_validated_dd          = 'field_validated_dd';
var field_validated_amount      = 'field_validated_amount';
var field_validated_amount2     = 'field_validated_amount2';
var field_validated_date        = 'field_validated_date';
var field_validated_freq        = 'field_validated_freq';
var field_validated_address     = 'field_validated_address';
var field_validated_cce         = 'field_validated_cce';
var field_validated_AADF        = 'field_validated_AADF';
var field_validated_pdd         = 'field_validated_pdd';
var field_validated_age         = 'field_validated_age';

var field_amount_rg                 = 'field_amount_rg';
var field_amount_rg_upg             = 'field_amount_rg_upg';
var field_amount_cash               = 'field_amount_cash';
var field_amount_2                  = 'field_amount_2';
var field_payment_method            = 'field_payment_method';
var field_payment_frequency_new     = 'field_payment_frequency_new';
var field_payment_date_new          = 'field_payment_date_new';
var field_defered_months            = 'field_defered_months';
var field_address_1                 = 'field_address_1';
var field_address_city              = 'field_address_city';
var field_address_state             = 'field_address_state';
var field_address_post_code         = 'field_address_post_code';
var field_donor_title               = 'field_donor_title';
var field_dob                       = 'field_dob';


var field_dd_name       = 'field_dd_name';
var field_dd_number     = 'field_dd_number';
var field_dd_bsb        = 'field_dd_bsb';
var field_cc_expiry_old = 'field_cc_expiry_old';

var field_annual_amount     = 'field_annual_amount'; 
var field_total_amount      = 'field_total_amount';
var field_total_upg_amount  = 'field_total_upg_amount';

var label_segment                   = 'label_segment';
var label_payment_date              = 'label_payment_date';
var label_payment_date_new          = 'label_payment_date_new';
var label_payment_frequency         = 'label_payment_frequency';
var label_payment_frequency_new     = 'label_payment_frequency_new';
var label_payment_method            = 'label_payment_method';
var label_donor_title               = 'label_donor_title';
var label_validated_pdd             = 'label_validated_pdd';
var label_defered_months            = 'label_defered_months';
var label_donor_spec_6              = 'label_donor_spec_6';
var label_old_rg                    = 'label_old_rg';
var label_record_id                 = 'label_record_id';
var label_call_id                   = 'label_call_id';
var label_payment_method_old        = 'label_payment_method_old';

var marker_pdd_status                       = 'marker_pdd_status';
var marker_address_status                   = 'marker_address_status';
var marker_variable_display                 = 'marker_variable_display';
var marker_variable_display_dec             = 'marker_variable_display_dec';
var marker_variable_display_freq            = 'marker_variable_display_freq';
var marker_variable_display_default         = 'marker_variable_display_default';
var marker_variable_display_freq_default    = 'marker_variable_display_freq_default';
var marker_ps_button_rg                     = 'marker_ps_button_rg';
var marker_ps_button_cash                   = 'marker_ps_button_cash';

var div_payment_dd  = 'div_payment_dd';
var div_payment_cc  = 'div_payment_cc';
var div_payment_dde = 'div_payment_dde';
var div_payment_cce = 'div_payment_cce';

var id_payment_modal_dec = 'declinesPaymentModalCC';

function OnLoad() {
    InitializeStyles();
    InitilaizePageLoadTriggers();
    PopulateAndValidateFunctions();
    PupulateDonorTitleBox();
    (ElementGet(option_address_required).innerText = '1') && InitializeAddressValidation();
    DisableAutoCompletes();
}

function InitializeStyles() {
    var frameStyle = document.createElement("style");
    frameStyle.type = "text/css"
    var frameCssStyle =
        ".onload-hide { display: none !important ; } " + ".onload-noBorder { border-style: none !important ; } " + ".btn-header { height: 30px ; margin-top: auto ; margin-bottom: auto ; } " +
        ".terminate-options { zoom: 1.2 ; color: white ; } " + ".terminate-options:hover { zoom: 1.5 ; color: ghostwhite ; } " + "html { scroll-behavior: smooth ; } " +
        ".content-slab { background-color: #f4f5f5 ; padding: 15px ; border-radius: 5px ; box-shadow: #ababab 1px 1px 2px 1px ; margin: 20px ; margin-bottom: 0 ; } ";
    frameStyle.innerHTML = frameCssStyle;
    document.getElementsByTagName("head")[0].appendChild(frameStyle);

    var fontAwesome = document.createElement("link");
    fontAwesome.rel = "stylesheet";
    fontAwesome.href = "https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css";
    document.getElementsByTagName("head")[0].appendChild(fontAwesome);

    var pageStyle = document.createElement("style");
    pageStyle.type = "text/css"
    var pageCssStyle =
        `.mainbody { background-color: ${sys_style_col_background} !important ; } ` + ".contentarea { background-color: white !important ; padding: 0px !important ; border-style: groove !important ; } " +
        `#sidebar { background-color: ${sys_style_col_background} !important ; } ` + `.sidebar-offcanvas { background-color: ${sys_style_col_background} !important ; } ` +
        `#phonecontrol { background-color: ${sys_style_col_background} !important ; } ` + ".navbar { background-color: #00a6ff !important ; } " +
        ".navbar-brand > img { content: url('https://bitbucket.org/paretophonedev/contactspace_screenlibrary/downloads/icon-logo.svg') ; max-height: 20px ; margin: 0 ; } " +
        `.navbar-inverse .navbar-nav>li>a, .navbar-inverse .navbar-text { color: ${sys_style_col_navtext} !important ; } ` +
        "#calltimer > span { color: #ffe000 !important ; font-weight: bold !important ; } " + `body { background-color: ${sys_style_col_background} !important ; } `;
    pageStyle.innerHTML = pageCssStyle;
    window.top.document.getElementsByTagName("head")[0].appendChild(pageStyle);
}

function ScrollToTop() {
    document.body.scrollTop = 0;
    document.documentElement.scrollTop = 0;
}

function ElementExists(className) {
    return document.getElementsByClassName(className).length > 0 ? true : false;
}

function ElementGet(className) {
    var returnResult = document.getElementsByClassName(className);
    return returnResult.length > 0 ? returnResult[0] : null;
}

function ElementGetAll(className) {
    var returnResult = document.getElementsByClassName(className);
    return returnResult.length > 0 ? returnResult : null;
}

function ElementQueryGetAll(query) {
    var returnResult = document.querySelectorAll(query);
    return (returnResult && returnResult.length > 0) ? returnResult : null;
}

function ValidatedRegPattern(val, regPattern) {
    var matches = val.match(regPattern);
    return (matches != null && matches.length > 0) ? matches.join('') : null;
}

function InitilaizePageLoadTriggers() {
    ElementExists(trigger_using_variable_display_seg) && DisplayVariableScript();
    ElementExists(trigger_using_variable_display_freq) && DisplayVariableScriptFreq();
    ElementExists(trigger_using_variable_display_dec) && DisplayVariableScriptDec();
    ElementExists(trigger_validate_dd) && OnValidateDD();
}

function DisplayVariableScript() {
    var segement = ElementGet(label_segment).innerText;
    var marker = `${marker_variable_display}_${segement}`;
    if (!ElementExists(marker)) { marker = marker_variable_display_default; }
    var variableDisplays = ElementQueryGetAll(`.${marker_variable_display}`);
    if (variableDisplays) {
        variableDisplays.forEach(div => { div.style.display = div.classList.contains(marker) ? 'inherit' : 'none'; });
    }
}

function DisplayVariableScriptFreq() {
    var freq = ElementGet(label_payment_frequency_new).innerText.replace(' ', '');
    var marker = `${marker_variable_display_freq}_${freq}`;
    if (!ElementExists(marker)) { marker = marker_variable_display_freq_default; }
    var variableDisplays = ElementQueryGetAll(`.${marker_variable_display_freq}`);
    if (variableDisplays) {
        variableDisplays.forEach(div => { div.style.display = div.classList.contains(marker) ? 'inherit' : 'none'; });
    }
}

function DisplayVariableScriptDec() {
    var spec = ElementGet(label_donor_spec_6).innerText;
    var marker = `${marker_variable_display_dec}_${spec}`;
    var variableDisplays = ElementQueryGetAll(`.${marker_variable_display_dec}`);
    if (variableDisplays) {
        variableDisplays.forEach(div => { div.style.display = div.classList.contains(marker) ? 'inherit' : 'none'; });
    }
}

function CalculateAnnualAmount() {
    var amount = parseFloat(ElementGet(field_amount_rg).value);
    amount = CalculateAnnualAmountTotal(amount);
    ElementGet(field_annual_amount).value = amount;
    AutoUpdateScriptVarsSpan(amount, 'annual_amount');
}

function CalculateAnnualArreasAmount() {
    var amount = parseFloat(ElementGet(field_amount_rg).value);
    var amount2 = parseFloat(ElementGet(field_amount_2).value);
    amount = CalculateAnnualAmountTotal((amount + amount2));
    ElementGet(field_annual_amount).value = amount;
    AutoUpdateScriptVarsSpan(amount, 'annual_amount');
}

function CalculateAnnualUpgradeAmount(){
    var amount = parseFloat(ElementGet(field_amount_rg_upg).value);
    var amount2 = parseFloat(ElementGet(label_old_rg).innerText);
    if (amount2 == 'NaN') { amount2 = parseFloat(0.00).toFixed(2); }
    amount = CalculateAnnualAmountTotal((amount + amount2));
    ElementGet(field_annual_amount).value = amount;
    AutoUpdateScriptVarsSpan(amount, 'annual_amount');
}

function CalculateAnnualAmountTotal(amount) {
    var frequency = ElementGet(field_payment_frequency_new).value;
    var multiplier = 0;
    switch (frequency) {
        case 'Yearly': case 'Annual': case 'Annually': case 'Anually':
            multiplier = 1; break;
        case 'Biannual': case 'Bi-Annual': case 'Bi-annually': case 'Half Yearly': case 'Half-yearly': case 'Semi Annual': case 'Semi Annually': case 'Semi-Annual': case 'Semi-Annually': case 'Twice Yearly':
            multiplier = 2; break;
        case 'Tri-Yearly':
            multiplier = 3; break;
        case 'Quarterley': case 'Quarterly':
            multiplier = 4; break;
        case 'Month': case 'Monthly':
            multiplier = 12; break;
        case '4 Weekly': case 'Four - Weekly': case 'Four-Weekly': case 'Four Weekly': case 'Every 4 weeks':
            multiplier = 13.0446428571428; break;
        case 'Bi Monthly': case 'Bimonthly': case 'Bi-Monthly': case 'Semi-Monthly': case 'Half Monthly':
            multiplier = 24; break;
        case 'Biweekly': case 'Fortnightly':
            multiplier = 26.0892857142857; break;
        case 'Weekly':
            multiplier = 52.1785714285714; break;
        default:
            multiplier = 0; break;
    }
    return (amount * multiplier).toFixed(2);
}

function OnValidateAmountCash(el) {
    el.value = CalculateAmountFormated(el.value);
    AutoUpdateScriptVarsSpan(el.value, 'amount');
    CalculateAmountValidated(el, marker_ps_button_cash, option_min_cash, field_validated_amount);
}

function OnValidateAmountRG(el, isPageLoad) {
    el.value = CalculateAmountFormated(el.value);
    AutoUpdateScriptVarsSpan(el.value, 'amount');
    var cao = ElementGet(option_can_arrears_only);
    var min = (cao && cao.innerText == sys_validated_pass_keyword && el.value == 0 && ElementGet(field_amount_2).value > 0) ? 0 : option_min_rg;
    CalculateAmountValidated(el, marker_ps_button_rg, min, field_validated_amount);
    CalculateAnnualAmount();
    !isPageLoad && ValidatePDD(null, true);
}

function OnValidateAmountArrears(el, isPageLoad) {
    el.value = CalculateAmountFormated(el.value);
    var changeble = ElementGet(option_arrears_changeable).innerText;
    var oldRG = ElementGet(label_old_rg).innerText;
    var min = option_min_arrears;
    if (changeble != '1' && el.value > 0) {
        el.value = parseFloat(oldRG).toFixed(2);
        if (el.value == 'NaN') { el.value = Number.parseFloat(0.00).toFixed(2); }
    }
    else if (changeble == '1' && el.value == 0) {
        min = 0;
    }
    AutoUpdateScriptVarsSpan(el.value, 'amount_2');
    CalculateAmountValidated(el, marker_ps_button_rg, min, field_validated_amount2, true);
    CalculateAnnualArreasAmount();
    ElementGet(field_total_amount).value = el.value + Number(ElementGet(field_amount_rg).value);
    !isPageLoad && ValidatePDD(null, true);
}

function OnValidateUpgradeAmountRG(el, isPageLoad) {
    el.value = CalculateAmountFormated(el.value);
    AutoUpdateScriptVarsSpan(el.value, 'amount');
    CalculateAmountValidated(el, marker_ps_button_rg, option_min_rg, field_validated_amount);
    CalculateAnnualUpgradeAmount();
    ElementGet(field_total_upg_amount).value = Number.parseFloat((Number(el.value) + Number(ElementGet(label_old_rg).innerText))).toFixed(2);
    !isPageLoad && ValidatePDD(null, true);
}

function CalculateAmountValidated(el, psButton, minAmount, validated, isAmount2 = false) {
    psButton = ElementGet(psButton);
    if (isNaN(minAmount)) { minAmount = Number(ElementGet(minAmount).innerText); }
    validated = ElementGet(validated);
    var ok = (Number(el.value) >= minAmount) ? true : false;
    el.style.borderColor = ok ? sys_validation_colour_pass : sys_validation_colour_fail;
    validated.value = ok ? sys_validated_pass_keyword : sys_validated_fail_keyword;
    if (psButton) { psButton.style.display = ok ? 'initial' : 'none'; }
    if (isAmount2) { OnValidateAADF(null, validated, null, null, null); }
    else { OnValidateAADF(validated, null, null, null, null);}
}

function CalculateAmountFormated(val) {
    var returnVal = 0;
    var matched = ValidatedRegPattern(val, sys_regex_currency_pattern);
    if (matched) {
        if (matched.includes('.') && matched.slice(matched.length, -1) == '.') {
            matched += '0';
        }
        returnVal = parseFloat(matched).toFixed(2);
        if (returnVal == 'NaN') { returnVal = parseFloat(0.00).toFixed(2); }
    } else {
        returnVal = parseFloat(0.00).toFixed(2);
    }
    return returnVal;
}

function OnValidatePaymentMethod(el, isPageLoad) {
    OnPaymentOptionChange(el);
    var val = el.value;
    el.style.borderColor = (val == 'none') ? sys_validation_colour_fail : sys_validation_colour_pass;
    !isPageLoad && ValidatePDD(val, false);
    AutoUpdateScriptVarsSpan(((val == 'none') ? 'Not Selected' : val), 'payment_method');
}

function ValidatePDD(optionVal, resetPDD) {
    if (!optionVal) optionVal = ElementGet(field_payment_method).value;
    var el = ElementGet(marker_pdd_status);
    var pddRequired = (ElementGet(option_ppd_required_on_new).innerText == sys_validated_pass_keyword) ? true : false;
    var isDD = (optionVal.toLowerCase() == sys_payment_option_value_dd.toLowerCase()) ? true : false;
    var pddField = ElementGet(field_validated_pdd);
    if (pddRequired) {
        if (isDD) {
            if (resetPDD) {
                el.style.display = 'inherit';
                pddField.value = sys_validated_fail_keyword;
            }
            else {
                el.style.display = (pddField.value == sys_validated_pass_keyword) ? 'none' : 'inherit';
            }
        }
        else {
            el.style.display = 'none';
        }
    } else {
        el.style.display = 'none';
        pddField.value = sys_validated_pass_keyword;
    }
}

function OnValidateFrequency(el, isPageLoad) {
    var val = el.value;
    el.style.borderColor = (val == 'none') ? sys_validation_colour_fail : sys_validation_colour_pass;
    var validate = ElementGet(field_validated_freq);
    validate.value = (val == 'none') ? sys_validated_fail_keyword : sys_validated_pass_keyword;
    AutoUpdateScriptVarsSpan(((val == 'none') ? 'Not Selected' : val), 'payment_freq');
    !isPageLoad && ValidatePDD(null, true);

    if (ElementExists(field_amount_rg)) {
        CalculateAnnualAmount();
    }
    else {
        CalculateAnnualUpgradeAmount();
    }

    var dynamic = ElementGet(option_dynamic_payment_dates);
    if (dynamic && dynamic.innerHTML == sys_validated_pass_keyword) {
        PopulatePaymentDates();
        OnValidatePaymentDate(ElementGet(field_payment_date_new), isPageLoad);
    }

    OnValidateAADF(null, null, null, null, validate);
}

function OnValidatePaymentDate(el, isPageLoad) {
    var val = el.value;
    el.style.borderColor = (val == 'none') ? sys_validation_colour_fail : sys_validation_colour_pass;
    var validate = ElementGet(field_validated_date);
    validate.value = (val == 'none') ? sys_validated_fail_keyword : sys_validated_pass_keyword;
    AutoUpdateScriptVarsSpan(((val == 'none') ? 'Not Selected' : val), 'payment_date');
    !isPageLoad && ValidatePDD(null, true);
    OnValidateAADF(null, null, null, validate, null);
}

function OnValidatePaymentDeferral(el, isPageLoad) {
    var val = el.value;
    if (val == 'none') {
        AutoUpdateScriptVarsSpan('0 Months', 'payment_defer_full');
        AutoUpdateScriptVarsSpan('0', 'payment_defer_number');
    }
    else {
        AutoUpdateScriptVarsSpan(val, 'payment_defer_number');
        val = val + ' Month' + (val != 1 ? 's' : '');
        AutoUpdateScriptVarsSpan(val, 'payment_defer_full');
    }
    !isPageLoad && ValidatePDD(null, true);
}

function AutoUpdateScriptVarsSpan(val, keyword) {
    document.querySelectorAll(keyword_auto_update + keyword).forEach(span => { span.innerText = val; });
}

function InitializeAddressValidation(){
    var els = {
        address: ElementGet(field_address_1),
        city: ElementGet(field_address_city),
        state: ElementGet(field_address_state),
        code: ElementGet(field_address_post_code),
        marker: ElementGet(marker_address_status),
        validated: ElementGet(field_validated_address)
    };

    els.address.addEventListener('change', () => { CalculateAddressValidation(els) });
    els.city.addEventListener('change', () => { CalculateAddressValidation(els) });
    els.state.addEventListener('change', () => { CalculateAddressValidation(els) });
    els.code.addEventListener('change', () => { CalculateAddressValidation(els) });
    CalculateAddressValidation(els);
}

function CalculateAddressValidation(els) {
    var isValidated = (els.address.value && els.city.value && els.state.value && els.code.value) ? true : false;
    els.validated.value = isValidated ? sys_validated_pass_keyword : sys_validated_fail_keyword;
    if (els.marker) { els.marker.style.display = (els.validated.value == '1') ? "none" : "inherit"; }
    OnValidateAADF(null, null, els.validated, null, null);
}

function OnValidateDD()
{
    var els = {
        name: ElementGet(field_dd_name),
        number: ElementGet(field_dd_number),
        bsb: ElementGet(field_dd_bsb),
        validated: ElementGet(field_validated_dd)
    };

    els.name.addEventListener('input', () => {
        CalulculateValidationDD(els);
    });

    els.number.addEventListener('input', () => {
        CalulculateValidationDD(els);
    });

    els.bsb.addEventListener('input', () => {
        var inputVal = els.bsb.value;
        var matched = ValidatedRegPattern(inputVal, sys_regex_number_pattern);
        inputVal = (matched) ? matched : '';
        els.bsb.value = (inputVal.length < 4) ? inputVal : (inputVal.slice(0, 3) + '-' + inputVal.slice(3));
        els.bsb.value = els.bsb.value.substring(0, 7);
        CalulculateValidationDD(els);
    });

    CalulculateValidationDD(els);
}

function CalulculateValidationDD(els) {
    var pass = true;

    if (els.name && els.name.value.length > 4) {
        els.name.style.borderColor = sys_validation_colour_pass;
    }
    else {
        els.name.style.borderColor = sys_validation_colour_fail;
        pass = false;
    }

    if (els.bsb && els.bsb.value.length == 7) {
        els.bsb.style.borderColor = sys_validation_colour_pass;
    }
    else {
        els.bsb.style.borderColor = sys_validation_colour_fail;
        pass = false;
    }

    if (els.number && els.number.value.length > 5 && els.number.value.length < 14) {
        els.number.style.borderColor = sys_validation_colour_pass;
    }
    else {
        els.number.style.borderColor = sys_validation_colour_fail;
        pass = false;
    }

    els.validated.value = pass ? sys_validated_pass_keyword : sys_validated_fail_keyword;
}

function OnValidateCCE(inputEl, validateEL)
{
    var inputVal = inputEl.value;
    var matched = ValidatedRegPattern(inputVal, sys_regex_number_pattern);
    inputVal = (matched) ? matched : '';
    inputEl.value = (inputVal.length < 3) ? inputVal : (inputVal.slice(0, 2) + '/' + inputVal.slice(2));
    inputEl.value = inputEl.value.substring(0, 5);

    var pass = inputEl.value.length == 5 ? true : false;
    if (pass) {
        var d = new Date();
        var monthField = inputEl.value.slice(0, 2);
        monthField = Number(monthField);
        var yearField = inputEl.value.slice(3);
        var curYear = d.getFullYear().toString();

        pass = (monthField > 0 && monthField < 13) ? true : false;

        if (pass) {
            var fieldDate = new Date(curYear.slice(0, 2) + yearField, Number(monthField) - 1);
            var curDate = new Date(curYear, d.getMonth());

            pass = (fieldDate >= curDate) ? true : false;
        }
    }

    inputEl.style.borderColor = pass ? sys_validation_colour_pass : sys_validation_colour_fail;
    validateEL.value = pass ? sys_validated_pass_keyword : sys_validated_fail_keyword;
}

function PupulateDonorTitleBox() {
    var label = ElementGet(label_donor_title).innerText;
    var field = ElementGet(field_donor_title);
    for (var i = 0; i < field.options.length; i++) { field.options[i].selected = (field.options[i].value == label) ? true : false; }
}

function PopulatePaymentOptions() {
    var method = ElementGet(option_payment_option_style).value;
    var select = ElementGet(field_payment_method);
    var current = ElementGet(label_payment_method).innerText;
    var canSendForm = ElementGet(option_can_send_form) != null ? true : false;
    switch (method) {
        case 'CON-Standard': case 'RCT-Standard': case 'UPG-Standard':
            select.add(CreateSelectOption(sys_payment_option_value_cc, sys_payment_option_value_cc, current));
            select.add(CreateSelectOption(sys_payment_option_value_dd, sys_payment_option_value_dd, current));
            break;
        case 'CON-Existing': case 'RCT-Existing': case 'UPG-Existing':
            select.add(CreateSelectOption(sys_payment_option_value_cc, sys_payment_option_value_cc, current));
            select.add(CreateSelectOption(sys_payment_option_value_dd, sys_payment_option_value_dd, current));
            var old = ElementGet(label_payment_method_old).innerText;
            if (old.toLowerCase() == sys_payment_option_value_dd.toLowerCase()) { select.add(CreateSelectOption(sys_payment_option_value_dde, sys_payment_option_value_dde, current)); }
            else if (old.toLowerCase() == sys_payment_option_value_cc.toLowerCase()) { select.add(CreateSelectOption(sys_payment_option_value_cce, sys_payment_option_value_cce, current)); }
            break;
        case 'DEC-EHS':
            var old = (ElementGet(label_payment_method_old).innerText.toUpperCase() == sys_payment_option_value_cc.toUpperCase()) ? sys_payment_option_value_cc : sys_payment_option_value_dd;
            select.add(CreateSelectOption(old, old, current));
            if (ElementGet(label_donor_spec_6).innerText == 'Soft') {
                var existing = (old.toLowerCase() == sys_payment_option_value_cc.toLowerCase()) ? sys_payment_option_value_cce : sys_payment_option_value_dde;
                select.add(CreateSelectOption(existing, existing, current));
            }
            break;
        default:
            console.error(`PopulatePaymentOptions Failed - Method not Matched : ${method}`);
            break;
    }
    if (canSendForm && ElementGet(option_can_send_form).innerText == sys_validated_pass_keyword) {
        select.add(CreateSelectOption(sys_payment_option_value_sf, sys_payment_option_value_sf, current));
    }
}

function PopulateDeferralMonths() {
    var optionMonths = parseInt(ElementGet(option_deferral_months).innerText);
    if (optionMonths < 1) { return; }

    var select = ElementGet(field_defered_months);
    var current = ElementGet(label_defered_months).innerText;
    var canChange = (ElementGet(option_deferral_changeable).innerText == sys_validated_pass_keyword) ? true : false;

    if (!canChange && current) {
        select.innerHTML = '';
        var t = current + ' Month' + (current != 1 ? 's' : '');
        select.add(CreateSelectOption(current, t, current));
    }
    else {
        for (var i = 1; i <= optionMonths; i++) {
            var t = i + ' Month' + (i != 1 ? 's' : '');
            select.add(CreateSelectOption(i, t, current));
        }
    }
}

function PopulatePaymentFrequencies() {
    var select = ElementGet(field_payment_frequency_new);
    var canChange = (ElementGet(option_payment_frequency_changeable).innerText == sys_validated_pass_keyword) ? true : false;
    var freqOld = ElementGet(label_payment_frequency).innerText;
    var freqNew = ElementGet(label_payment_frequency_new).innerText;

    var custom_no4w2m = ElementExists(custom_scripting_no_4w2m) ? true : false;
    var paymentOverride = ElementGet(option_payment_frequencie_override);

    if (paymentOverride && paymentOverride.innerText.length > 1) {
        select.add(CreateSelectOption(paymentOverride.innerText, paymentOverride.innerText, paymentOverride.innerText));
        return;
    }

    if (!canChange && freqOld) {
        select.innerHTML = "";
        select.add(CreateSelectOption(freqOld, freqOld, freqOld));
    }
    else {
        var options = ElementGet(option_payment_frequencies_available).innerText.split(';');
        options.forEach(freq => {
            if (custom_no4w2m && freq == 'Monthly' && freqOld == '4 Weekly') { }
            else {
                select.add(CreateSelectOption(freq, freq, freqNew));
            }
        });
    }
}

function PopulatePaymentDates() {
    var select = ElementGet(field_payment_date_new);
    var canChange = (ElementGet(option_payment_dates_changeable).innerText == sys_validated_pass_keyword) ? true : false;
    var dateOld = ElementGet(label_payment_date).innerText;
    var dateNew = ElementGet(label_payment_date_new).innerText;

    if (!canChange && dateOld) {
        select.innerHTML = "";
        select.add(CreateSelectOption(dateOld, dateOld, dateOld));
    }
    else {
        select.innerHTML = "";
        select.add(CreateSelectOption('none', '', dateNew));

        var optionsClass;
        var dynamic = ElementGet(option_dynamic_payment_dates);
        if (dynamic && dynamic.innerHTML == sys_validated_pass_keyword)
        {
            var freq = ElementGet(field_payment_frequency_new).value;
            switch (freq) {
                case 'Month': case 'Monthly':
                    optionsClass = option_dynamic_payment_dates_Month;
                    break;
                case '4 Weekly': case 'Four - Weekly': case 'Four-Weekly': case 'Four Weekly': case 'Every 4 weeks':
                    optionsClass = option_dynamic_payment_dates_FourWK;
                    break;
                case 'None': case 'none':
                    break;
                default:
                    console.warn(`Dynamic date not matches with freq ${freq}, using defaults`);
                    break;
            }
        }
        else
        {
            optionsClass = option_payment_dates_available;
        }
        if (optionsClass) {
            var options = ElementGet(optionsClass);
            if (options) {
                options = options.innerText.split(';');
                options.forEach(date => {
                    select.add(CreateSelectOption(date, date, dateNew));
                });
            }
        }
    }
}

function CreateSelectOption(val, text, current) {
    var option = document.createElement('option');
    option.value = val;
    option.text = text;
    option.selected = (val == current);
    return option;
}

function OnPaymentOptionChange(el) {
    var val = el.value;
    var dd = ElementGet(div_payment_dd);
    var dde = ElementGet(div_payment_dde);
    var cc = ElementGet(div_payment_cc);
    var cce = ElementGet(div_payment_cce);
    if (dd) { dd.style.display = (val.toLowerCase() == sys_payment_option_value_dd.toLowerCase()) ? "inherit" : "none"; }
    if (dde) { dde.style.display = (val.toLowerCase() == sys_payment_option_value_dde.toLowerCase()) ? "inherit" : "none"; }
    if (cc) { cc.style.display = (val.toLowerCase() == sys_payment_option_value_cc.toLowerCase()) ? "inherit" : "none"; }
    if (cce) { cce.style.display = (val.toLowerCase() == sys_payment_option_value_cce.toLowerCase()) ? "inherit" : "none"; }

    var arrearsAny = ElementGet(option_arrears_any);
    if (arrearsAny)
    {
        var amountField2 = ElementGet(field_amount_2);
        if (arrearsAny.innerText == sys_validated_pass_keyword) { amountField2.readOnly = false; }
        else {
            var enable = false;
            switch (val) {
                case sys_payment_option_value_cc: enable = (ElementGet(option_arrears_cc).innerText == sys_validated_pass_keyword) ? true : false; break;
                case sys_payment_option_value_cce: enable = (ElementGet(option_arrears_cce).innerText == sys_validated_pass_keyword) ? true : false; break;
                case sys_payment_option_value_dd: enable = (ElementGet(option_arrears_dd).innerText == sys_validated_pass_keyword) ? true : false; break;
                case sys_payment_option_value_dde: enable = (ElementGet(option_arrears_dde).innerText == sys_validated_pass_keyword) ? true : false; break;
                case 'None': case 'none': enable = false; break;
                default: enable = false; console.log('ERROR: OnPaymentOptionChange() method not matched.'); break;
            }
            if (enable) {
                amountField2.readOnly = false;
            }
            else {
                amountField2.readOnly = true;
                amountField2.value = '0.00';
            }
        }
    }
}

function OnValidateAADF(amount, amount2, address, date, freq) {
    if (amount) { amount = amount.value; }
    else {
        amount = ElementGet(field_validated_amount);
        amount = (amount) ? amount.value : sys_validated_pass_keyword;
    }

    if (amount2) { amount2 = amount2.value; }
    else {
        amount2 = ElementGet(field_validated_amount2);
        amount2 = (amount2) ? amount2.value : sys_validated_pass_keyword;
    }

    if (address) { address = address.value; }
    else {
        address = ElementGet(field_validated_address);
        address = (address) ? address.value : sys_validated_pass_keyword;
    }

    if (freq) { freq = freq.value; }
    else {
        freq = ElementGet(field_validated_freq);
        freq = (freq) ? freq.value : sys_validated_pass_keyword;
    }

    if (date) { date = date.value; }
    else {
        date = ElementGet(field_validated_freq);
        date = (date) ? date.value : sys_validated_pass_keyword;
    }

    var validated = ElementGet(field_validated_AADF);
    validated.value = (
        amount == sys_validated_pass_keyword &&
        amount2 == sys_validated_pass_keyword &&
        address == sys_validated_pass_keyword &&
        freq == sys_validated_pass_keyword &&
        date == sys_validated_pass_keyword)
        ? sys_validated_pass_keyword : sys_validated_fail_keyword;
}

function PopulateAndValidateFunctions() {
    var defer = ElementGet(field_defered_months)
    if (defer) {
        PopulateDeferralMonths();
        OnValidatePaymentDeferral(defer, true);
    }

    var cce = ElementGet(field_cc_expiry_old);
    var ccev = ElementGet(field_validated_cce);
    if (cce && ccev) {
        cce.addEventListener('input', () => {
            OnValidateCCE(cce, ccev);
        });
        OnValidateCCE(cce, ccev);
    }

    var amountRG = ElementGet(field_amount_rg);
    var amount2 = ElementGet(field_amount_2);

    if (amountRG && amount2) {
        amountRG.addEventListener('change', () => {
            OnValidateAmountRG(amountRG, false);
            OnValidateAmountArrears(amount2, false);
        });
        amount2.addEventListener('change', () => {
            OnValidateAmountRG(amountRG, false);
            OnValidateAmountArrears(amount2, false);
        });
        OnValidateAmountArrears(amount2, false);
        OnValidateAmountRG(amountRG, true);
    }
    else {
        if (amountRG) {
            amountRG.addEventListener('change', () => {
                OnValidateAmountRG(amountRG, false);
            });
            OnValidateAmountRG(amountRG, true);
        }


        if (amount2) {
            amount2.addEventListener('change', () => {
                OnValidateAmountArrears(amount2, false);
            });
            OnValidateAmountArrears(amount2, false);
        }
    }

    var amountRG_U = ElementGet(field_amount_rg_upg);
    if (amountRG_U) {
        amountRG_U.addEventListener('change', () => {
            OnValidateUpgradeAmountRG(amountRG_U, false);
        });
        OnValidateUpgradeAmountRG(amountRG_U, false);
    }

    var amountCash = ElementGet(field_amount_cash);
    if (amountCash) {
        amountCash.addEventListener('change', () => {
            OnValidateAmountCash(amountCash);
        });
        OnValidateAmountCash(amountCash);
    }

    var paymentMethod = ElementGet(field_payment_method);
    if (paymentMethod){
        PopulatePaymentOptions();
        OnValidatePaymentMethod(paymentMethod, false);
    }

    var paymentFreq = ElementGet(field_payment_frequency_new);
    if (paymentFreq) {
        PopulatePaymentFrequencies();
        OnValidateFrequency(paymentFreq, true);
    }

    var paymentDate = ElementGet(field_payment_date_new);
    if (paymentDate){
        PopulatePaymentDates();
        OnValidatePaymentDate(paymentDate, true);
    }

    var dob = ElementGet(field_dob);
    if (dob) {
        var validate = ElementGet(field_validated_age);
        dob.addEventListener('change', () => {
            ValidateDOB(dob.value, validate);
        });
        ValidateDOB(dob.value, validate);
    }
}

function ValidateDOB(date, validate) {
    if (date) {
        var currentDate = new Date();
        var donorDate = new Date(date);
        var years = currentDate.getFullYear() - donorDate.getFullYear();
        var pass = years >= 18 ? true : false;
        if (!pass) { alert('This person is too young, must be 18 or over. Will not be able to process RG'); }
        validate.value = pass ? sys_validated_pass_keyword : sys_validated_fail_keyword;
    } else {
        validate.value = sys_validated_pass_keyword;
    }
}

function LoadPayshieldRG(el) {
    var amount = ElementGet(field_amount_rg).value;
    CallPayshieldWindow(amount, el, option_ps_instance);
}

function LoadPayshieldUPG(el) {
    var amount = ElementGet(field_amount_rg_upg).value;
    CallPayshieldWindow(amount, el, option_ps_instance);
}

function LoadPayshieldCash(el) {
    var amount = ElementGet(field_amount_cash).value;
    CallPayshieldWindow(amount, el, option_ps_instance);
}

function CallPayshieldWindow(amount, el, instance) {
    var mode = ElementGet('option_dial_mode');
    if (mode) {
        console.log(mode.innerText);
        switch (mode.innerText) {
            case '3':
                PayshieldActionPredictiveMode(amount, el, instance);
                console.log('Predictive');
                break;
            default:
                PayshieldActionPreviewMode(amount, el, instance);
                break;
        }
    }
    else {
        PayshieldActionPreviewMode(amount, el, instance);
    }
}

function PayshieldActionPredictiveMode(amount, el, instance) {
    var recordID = ElementGet(label_record_id).innerText;
    var callID = ElementGet(label_call_id).innerText;
    var payShieldInstance = ElementGet(instance).innerText;
    var postData = {
        callid: callID,
        instancename: payShieldInstance,
        dialmode: 3
    };

    amount = (amount * 100);

    var url =
        `helpers/payshield_dev.html?cscallid=${callID}&iframehost=91d1f175a3&instance=${payShieldInstance}&amount=${amount}&display_amount_field=0` +
        `&display_cvv_field=0&customer_reference_number1=${callID}&display_customer_reference_number1_field=0` +
        `&customer_reference_number2=${recordID}&customer_reference_number3=TBD`;

    $.post(
        "helpers/payshieldtransfer_amd.php",
        postData,
        function () {
            window.open(url, 'contactSPACE', 'width=800,height=650');
        }
    );


}

function PayshieldActionPreviewMode(amount, el, instance) {
    var recordID = ElementGet(label_record_id).innerText;
    var callID = ElementGet(label_call_id).innerText;
    var payShieldInstance = ElementGet(instance).innerText;
    var postData = {
        callid: callID,
        instancename: payShieldInstance,
        dialmode: 0
    };

    amount = (amount * 100);

    var url =
        `helpers/payshield_dev.html?cscallid=${callID}&iframehost=91d1f175a3&instance=${payShieldInstance}&amount=${amount}&display_amount_field=0` +
        `&display_cvv_field=0&customer_reference_number1=${callID}&display_customer_reference_number1_field=0` +
        `&customer_reference_number2=${recordID}&customer_reference_number3=TBD`;

    $.post(
        "helpers/payshieldtransfervce1.php",
        postData,
        function () {
            window.open(url, 'contactSPACE', 'width=800,height=650');
        }
    );
}

function LoadPayshieldDeclines(){
    var validated = ElementGet(field_validated_amount).value == sys_validated_pass_keyword ? true : false;
    var validated2 = ElementGet(field_validated_amount2).value == sys_validated_pass_keyword ? true : false;
    if (validated == false) {
        alert('Cannot Process through Merchant Warrior. Amount not validated.');
        return;
    }
    if (validated2 == false) {
        alert('Cannot Process through Merchant Warrior. Arrears amount not validated.');
        return;
    }
    var amount = ElementGet(field_amount_rg).value;
    if (amount > 0) {
        $('#' + id_payment_modal_dec).modal('hide');
        CallPayshieldWindow(amount, null, option_ps_instance);
    }
    else {
        alert('Cannot Process through Merchant Warrior. Amount cannot be $0');
    }
}

function LoadPayshieldDeclinesMW() {
    var validated = ElementGet(field_validated_amount).value == sys_validated_pass_keyword ? true : false;
    var validated2 = ElementGet(field_validated_amount2).value == sys_validated_pass_keyword ? true : false;
    if (validated == false) {
        alert('Cannot Process through Merchant Warrior. Amount not validated.');
        return;
    }
    if (validated2 == false) {
        alert('Cannot Process through Merchant Warrior. Arrears amount not validated.');
        return;
    }
    var amount = ElementGet(field_amount_2).value;
    if (amount > 0) {
        $('#' + id_payment_modal_dec).modal('hide');
        CallPayshieldWindow(amount, null, option_mw_instance);
    }
    else {
        alert('Cannot Process through Merchant Warrior. Arrears amount cannot be $0');
    }
}

function DisableAutoCompletes() {
    ElementQueryGetAll('input').forEach(input => { input.autocomplete = "off"; });
}