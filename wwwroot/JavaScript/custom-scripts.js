var sys_regex_number_pattern        = /\d+/g;
var sys_regex_currency_pattern      = /^\d*.?\d*$/;
var sys_payment_option_value_cc     = 'Credit Card';
var sys_payment_option_value_dd     = 'Direct Debit';
var sys_validation_colour_fail      = 'indianred';
var sys_validation_colour_pass      = '#0ad80a';
var sys_style_col_background        = '#d7d7d7';
var sys_style_col_navtext           = 'white';
var sys_validated_pass_keyword      = '1';
var sys_validated_fail_keyword      = '0';
var sys_auto_update_path            = 'span.auto_update_';

var trigger_validate_dd                     = 'trigger_validate_dd';
var trigger_validate_amount                 = 'trigger_validate_amount';
var trigger_validate_amount_cash            = 'trigger_validate_amount_cash';
var trigger_validate_payment_method         = 'trigger_validate_payment_method';
var trigger_validate_payment_frequency      = 'trigger_validate_payment_frequency';
var trigger_validate_payment_date           = 'trigger_validate_payment_date';
var trigger_using_variable_display_seg          = 'trigger_using_variable_display';
var marker_variable_display                 = 'marker_variable_display';
var marker_variable_display_default         = 'marker_variable_display_default';

var field_dd_name                   = 'field_dd_name';
var field_dd_bsb                    = 'field_dd_bsb';
var field_dd_number                 = 'field_dd_number';
var field_cce_expiry                = 'field_cce_expiry';
var field_payment_method            = 'field_payment_method';
var field_payment_date_new          = 'field_payment_date_new'
var field_payment_frequency_new     = 'field_payment_frequency_new';
var field_amount                    = 'field_amount';
var field_defered_months            = 'field_defered_months';
var field_validated_dd              = 'field_validated_dd';
var field_validated_amount          = 'field_validated_amount';
var field_validated_pdd             = 'field_validated_pdd';
var field_validated_date            = 'field_validated_date';
var field_validated_freq            = 'field_validated_freq';
var field_validated_address         = 'field_validated_address';
var field_address_1                 = 'field_address_1';
var field_address_city              = 'field_address_city';
var field_address_state             = 'field_address_state';
var field_address_post_code         = 'field_address_post_code';
var field_donor_title               = 'field_donor_title';

var label_payment_date          = 'label_payment_date';
var label_payment_date_new      = 'label_payment_date_new';
var label_payment_frequency     = 'label_payment_frequency';
var label_payment_frequency_new = 'label_payment_frequency_new';
var label_payment_method        = 'label_payment_method';
var label_defered_months        = 'label_defered_months';
var label_segment               = 'label_segment';
var label_donor_title           = 'label_donor_title';

var marker_pdd_status       = 'marker_pdd_status';
var marker_address_status   = 'marker_address_status';
var marker_ps_button_cash   = 'marker_ps_button_cash';
var marker_ps_button_rg     = 'marker_ps_button_rg';

var option_min_rg                           = 'option_min_rg';
var option_min_cash                         = 'option_min_cash';
var option_payment_option_style             = 'option_payment_style';
var option_payment_dates_changeable         = 'option_payment_dates_changeable';
var option_payment_dates_available     	    = 'option_payment_dates_available';
var option_payment_frequency_changeable     = 'option_payment_frequency_changeable';
var option_payment_frequencies_available    = 'option_payment_frequencies_available';
var option_deferral_changeable              = 'option_deferral_changeable';
var option_deferral_months                  = 'option_deferral_months';

var div_payment_dd_new = 'div_payment_dd_new';
var div_payment_cc_new = 'div_payment_cc_new';


function OnLoad()
{
    InitializeStyles();
    InitializeValidations();

    PopulatePaymentOptions();
    PopulatePaymentDates();
    PopulatePaymentFrequency();
    PopulateDeferralMonths();
    PopulateDonorTitle();

    CheckVariableDisplay();
}

function InitializeStyles()
{
    var frameStyle = document.createElement("style");
    frameStyle.type = "text/css"
    var frameCssStyle =
        ".onload-hide { display: none !important ; } " +
        ".onload-noBorder { border-style: none !important ; } " +
        ".btn-header { height: 30px ; margin-top: auto ; margin-bottom: auto ; } " +
        ".terminate-options { zoom: 1.2 ; color: white ; } " +
        ".terminate-options:hover { zoom: 1.5 ; color: ghostwhite ; } " +
        "html { scroll-behavior: smooth ; } " +
        ".content-slab { background-color: #f4f5f5 ; padding: 15px ; border-radius: 5px ; box-shadow: #ababab 1px 1px 2px 1px ; margin: 20px ; margin-bottom: 0 ; } ";
    frameStyle.innerHTML = frameCssStyle;
    document.getElementsByTagName("head")[0].appendChild(frameStyle);

    var fontAwesome = document.createElement("link");
    fontAwesome.rel = "stylesheet";
    fontAwesome.href = "https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css";
    document.getElementsByTagName("head")[0].appendChild(fontAwesome);

    var pageStyle = document.createElement("style");
    pageStyle.type = "text/css"
    var pageCssStyle =
        `.mainbody { background-color: ${sys_style_col_background} !important ; } ` +
        ".contentarea { background-color: white !important ; padding: 0px !important ; border-style: groove !important ; } " +
        `#sidebar { background-color: ${sys_style_col_background} !important ; } ` +
        `.sidebar-offcanvas { background-color: ${sys_style_col_background} !important ; } ` +
        `#phonecontrol { background-color: ${sys_style_col_background} !important ; } ` +
        ".navbar { background-color: #00a6ff !important ; } " +
        ".navbar-brand > img { content: url('https://bitbucket.org/paretophonedev/contactspace_screenlibrary/downloads/icon-logo.svg') ; max-height: 20px ; margin: 0 ; } " +
        `.navbar-inverse .navbar-nav>li>a, .navbar-inverse .navbar-text { color: ${sys_style_col_navtext} !important ; } ` +
        "#calltimer > span { color: #ffe000 !important ; font-weight: bold !important ; } " +
        `body { background-color: ${sys_style_col_background} !important ; } `;
    pageStyle.innerHTML = pageCssStyle;
    window.top.document.getElementsByTagName("head")[0].appendChild(pageStyle);
}

function ReimagePhone()
{
    window.top.document.getElementById('callcontrolframe').contentWindow.document.getElementById('phonecontrol').style.backgroundImage = "url('https://bitbucket.org/paretophonedev/contactspace_screenlibrary/downloads/mobilePhoneSprite.png')";
}

function ScrollToTop() {
    document.body.scrollTop = 0;
    document.documentElement.scrollTop = 0;
}

function OnPaymentOptionChange(selectElement)
{
    var val = selectElement.value;
    document.getElementsByClassName(div_payment_dd_new)[0].style.display = (val == sys_payment_option_value_dd) ? "inherit" : "none";
    document.getElementsByClassName(div_payment_cc_new)[0].style.display = (val == sys_payment_option_value_cc) ? "inherit" : "none";
}

function InitializeValidations()
{
    if (document.getElementsByClassName(trigger_validate_dd).length > 0){
        AddValidationListeners_DirectDebit();
    }

    if (document.getElementsByClassName(trigger_validate_amount).length > 0) {
        AddValidationListeners_Amount(true);
    }

    if (document.getElementsByClassName(trigger_validate_amount_cash).length > 0) {
        AddValidationListeners_Amount(false);
    }

    if (document.getElementsByClassName(trigger_validate_payment_method).length > 0) {
        AddValidationListeners_PaymentMethod();
    }

    if (document.getElementsByClassName(trigger_validate_payment_date).length > 0) {
        AddValidationListeners_PaymentDate();
    }

    if (document.getElementsByClassName(trigger_validate_payment_frequency).length > 0) {
        AddValidationListeners_PaymentFrequency();
    }

    if (document.getElementsByClassName(marker_address_status).length > 0) {
        AddValidationListeners_AddressDetails();
    }
}

function AddValidationListeners_PaymentDate()
{
    var field = document.getElementsByClassName(field_payment_date_new)[0];
    var validateField = document.getElementsByClassName(field_validated_date)[0];
    field.style.borderColor = (field.value == "none") ? sys_validation_colour_fail : sys_validation_colour_pass;

    field.addEventListener('change', () => {
        field.style.borderColor = (field.value == "none") ? sys_validation_colour_fail : sys_validation_colour_pass;
        var st = (field.value == "none") ? "(Select Payment Date)" : field.value;
        validateField.value = (field.value == "none") ? 0 : 1;
        document.querySelectorAll(sys_auto_update_path + 'payment_dates').forEach(p => {
            p.innerText = st;
        });
    });
}

function AddValidationListeners_PaymentMethod()
{
    var field = document.getElementsByClassName(field_payment_method)[0];
    field.style.borderColor = (field.value == "none") ? sys_validation_colour_fail : sys_validation_colour_pass;

    field.addEventListener('change', () => {
        field.style.borderColor = (field.value == "none") ? sys_validation_colour_fail : sys_validation_colour_pass;
        if (document.getElementsByClassName(marker_pdd_status).length > 0) {
            ShowValidation_PDD(field.value);
        }
    });
}

function AddValidationListeners_PaymentFrequency() {
    var field = document.getElementsByClassName(field_payment_frequency_new)[0];
    var validateField = document.getElementsByClassName(field_validated_freq)[0];
    field.style.borderColor = (field.value == "none") ? sys_validation_colour_fail : sys_validation_colour_pass;

    field.addEventListener('change', () => {
        field.style.borderColor = (field.value == "none") ? sys_validation_colour_fail : sys_validation_colour_pass;
        var st = (field.value == "none") ? "(Select Payment Frequency)" : field.value;
        validateField.value = (field.value == "none") ? 0 : 1;
        document.querySelectorAll(sys_auto_update_path + 'payment_frequency').forEach(p => {
            p.innerText = st;
        });
        CalculateAnualAmountRG(null, field);
    });
}

function AddValidationListeners_AddressDetails()
{
    var field_Address1 = document.getElementsByClassName(field_address_1)[0];
    var field_AddressCity = document.getElementsByClassName(field_address_city)[0];
    var field_AddressState = document.getElementsByClassName(field_address_state)[0];
    var field_AddressCode = document.getElementsByClassName(field_address_post_code)[0];
    var field_Validated = document.getElementsByClassName(field_validated_address)[0];
    var marker = document.getElementsByClassName(marker_address_status)[0];

    field_Address1.addEventListener('change', () => { Validate_Address(field_Address1, field_AddressCity, field_AddressState, field_AddressCode, field_Validated, marker) });
    field_AddressCity.addEventListener('change', () => { Validate_Address(field_Address1, field_AddressCity, field_AddressState, field_AddressCode, field_Validated, marker) });
    field_AddressState.addEventListener('change', () => { Validate_Address(field_Address1, field_AddressCity, field_AddressState, field_AddressCode, field_Validated, marker) });
    field_AddressCode.addEventListener('change', () => { Validate_Address(field_Address1, field_AddressCity, field_AddressState, field_AddressCode, field_Validated, marker) });
    Validate_Address(field_Address1, field_AddressCity, field_AddressState, field_AddressCode, field_Validated, marker);
}

function Validate_Address(one, city, state, code, validated, marker)
{
    var isValidated = (one.value && city.value && state.value && code.value) ? true : false;
    validated.value = isValidated ? sys_validated_pass_keyword : sys_validated_fail_keyword;
    marker.style.display = (validated.value == '1') ? "none" : "inherit";
}

function AddValidationListeners_DirectDebit()
{
    var name = document.getElementsByClassName(field_dd_name)[0];
    var number = document.getElementsByClassName(field_dd_number)[0];
    var bsb = document.getElementsByClassName(field_dd_bsb)[0];

    name.addEventListener('input', () => {
        ValidateFields_DirectDebit(name, number, bsb);
    });

    number.addEventListener('input', () => {
        var matches = number.value.match(sys_regex_number_pattern);
        number.value = (matches == null) ? '' : matches.join('');
        ValidateFields_DirectDebit(name, number, bsb);
    });

    bsb.addEventListener('input', () => {
        var inputVal = bsb.value;
        var matches = inputVal.match(sys_regex_number_pattern);
        inputVal = (matches == null) ? '' : inputVal.match(sys_regex_number_pattern).join('');
        bsb.value = (inputVal.length < 4) ? inputVal : (inputVal.slice(0, 3) + '-' + inputVal.slice(3));
        bsb.value = bsb.value.substring(0, 7);
        ValidateFields_DirectDebit(name, number, bsb);
    });

    ValidateFields_DirectDebit(name, number, bsb);
}

function ValidateFields_DirectDebit(name, number, bsb)
{
    var validatedField = document.getElementsByClassName(field_validated_dd)[0];

    var pass = true;

    if (name.value == '' || name.value == "NULL") {
        name.style.borderColor = sys_validation_colour_fail;
        pass = false;
    }
    else {
        name.style.borderColor = sys_validation_colour_pass;
    }

    if (bsb.value == '' || bsb.value == "NULL") {
        bsb.style.borderColor = sys_validation_colour_fail;
        pass = false;
    }
    else
        if (bsb.value.length != 7) {
        bsb.style.borderColor = sys_validation_colour_fail;
        pass = false;
    }
    else {
        bsb.style.borderColor = sys_validation_colour_pass;
    }

    if (number.value == '' || number.value == "NULL") {
        number.style.borderColor = sys_validation_colour_fail;
        pass = false;
    }
    else
        if (number.value.length < 5) {
        number.style.borderColor = sys_validation_colour_fail;
        pass = false;
    }
    else {
        number.style.borderColor = sys_validation_colour_pass;
    }

    validatedField.value = pass ? sys_validated_pass_keyword : sys_validated_fail_keyword;
}

function AddValidationListeners_Amount(forRG)
{
    var amountField = document.getElementsByClassName(field_amount)[0];
    var minAmount = (forRG == true) ? Number(document.getElementsByClassName(option_min_rg)[0].innerText) : Number(document.getElementsByClassName(option_min_cash)[0].innerText);
    var validateField = document.getElementsByClassName(field_validated_amount)[0];

    amountField.addEventListener('change', () => {
        var matches = amountField.value.match(sys_regex_currency_pattern);
        if (matches != null && matches.length > 0) {
            var matched = matches.join();
            if (matched.includes('.') && matched.slice(matched.length, -1) == '.') {
                matched = matched + '0';
            }
            amountField.value = parseFloat(matched).toFixed(2);
            if (amountField.value == 'NaN') { amountField.value = Number.parseFloat(0.00).toFixed(2); }
        }
        else
        {
            amountField.value = Number.parseFloat(0.00).toFixed(2);
        }
        document.querySelectorAll(sys_auto_update_path + 'amount').forEach(p => {
            p.innerText = amountField.value;
        });
        ValidateFields_Amount(amountField, minAmount, validateField, forRG);
        if (forRG) {
            CalculateAnualAmountRG(amountField, null);
        }
    });

    $(amountField).trigger("change");
    amountField.dispatchEvent(new Event("change"));
}

function ValidateFields_Amount(amountField, minRG, validateField, forRG)
{
    var button = document.getElementsByClassName((forRG ? marker_ps_button_rg : marker_ps_button_cash));
    button = (button && button.length > 0) ? button[0] : null;

    var amount = Number(amountField.value);

    if (amount != null && minRG != null && amount >= minRG) {
        amountField.style.borderColor = sys_validation_colour_pass;
        validateField.value = sys_validated_pass_keyword;
        if (button) {
            button.style.display = "initial";
        }
    }
    else {
        amountField.style.borderColor = sys_validation_colour_fail;
        validateField.value = sys_validated_fail_keyword;
        if (button) {
            button.style.display = "none";
        }
    }
}

function ShowValidation_PDD(selectedOption)
{
    label = document.getElementsByClassName(marker_pdd_status)[0];
    field = document.getElementsByClassName(field_validated_pdd)[0];
    if (selectedOption == sys_payment_option_value_dd) {
        label.style.display = (field.innerText == '1') ? "none" : "inherit";
    }
    else {
        label.style.display = 'none';
    }
}

function PopulatePaymentOptions()
{
    var method = document.getElementsByClassName(option_payment_option_style);
    var select = document.getElementsByClassName(field_payment_method);
    
    if (select.length == 0 || method.length == 0) { return; }

    method = method[0].innerText;
    select = select[0];

    var current = document.getElementsByClassName(label_payment_method)[0].innerText;

    switch (method) {
        case "standard_conversion":
            var option = document.createElement('option');
            option.val = sys_payment_option_value_cc;
            option.text = sys_payment_option_value_cc;
            option.selected = (sys_payment_option_value_cc == current);
            select.add(option);
            option = document.createElement('option');
            option.val = sys_payment_option_value_dd;
            option.text = sys_payment_option_value_dd;
            option.selected = (sys_payment_option_value_dd == current);
            select.add(option);
            break;
        default:
            console.log('PopulatePaymentOptions() - No valid option found');
            break;
    }

    $(select).trigger("change");
    select.dispatchEvent(new Event("change"));
}

function PopulatePaymentDates()
{
    var selectField = document.getElementsByClassName(field_payment_date_new);

    if (selectField == null || selectField.length == 0) { return; }

    selectField = selectField[0];

    var canChange = document.getElementsByClassName(option_payment_dates_changeable)[0].innerText;
    var dateOld = document.getElementsByClassName(label_payment_date)[0].innerText;
    var dateNew = document.getElementsByClassName(label_payment_date_new)[0].innerText;

    if (canChange != 'YES' && dateOld != null && dateOld != 'NULL') {
        selectField.innerHTML = "";
        var option = document.createElement('option');
        option.val = dateOld;
        option.text = dateOld;
        option.selected = true;
        selectField.add(option);
    }
    else {
        var options = document.getElementsByClassName(option_payment_dates_available)[0].innerText.split(';');
        options.forEach(date => {
            var option = document.createElement('option');
            option.val = date;
            option.text = date;
            option.selected = (dateNew == date);
            selectField.add(option);
        });
    }

    $(selectField).trigger("change");
    selectField.dispatchEvent(new Event("change"));
}

function PopulatePaymentFrequency()
{
    var selectField = document.getElementsByClassName(field_payment_frequency_new);

    if (selectField == null || selectField.length == 0) { return; }

    selectField = selectField[0];

    var canChange = document.getElementsByClassName(option_payment_frequency_changeable)[0].innerText;
    var paymentFreq = document.getElementsByClassName(label_payment_frequency)[0].innerText;
    var paymentFreqNew = document.getElementsByClassName(label_payment_frequency_new)[0].innerText;

    if (canChange != 'YES' && paymentFreq != null && paymentFreq != 'NULL') {
        selectField.innerHTML = "";
        var option = document.createElement('option');
        option.val = paymentFreq;
        option.text = paymentFreq;
        option.selected = true;
        selectField.add(option);
    }
    else {
        var options = document.getElementsByClassName(option_payment_frequencies_available)[0].innerText.split(';');
        options.forEach(freq => {
            var option = document.createElement('option');
            option.val = freq;
            option.text = freq;
            option.selected = (paymentFreqNew == freq);
            selectField.add(option);
        });
    }

    $(selectField).trigger("change");
    selectField.dispatchEvent(new Event("change"));
}

function PopulateDeferralMonths()
{
    var field = document.getElementsByClassName(field_defered_months);
    field = (field.length > 0) ? field[0] : null;

    if (field)
    {
        var current = Number.parseInt(document.getElementsByClassName(label_defered_months)[0].innerText);
        var optionCanChange = (document.getElementsByClassName(option_deferral_changeable)[0].innerText == 'YES') ? true : false;
        var optionMonths = Number.parseInt(document.getElementsByClassName(option_deferral_months)[0].innerText);

        if (optionMonths != 0)
        {
            if (optionCanChange == false && current)
            {
                field.innerHTML = "";
                var option = document.createElement('option');
                option.val = current;
                option.text = current + ' Month' + (current != 1 ? 's' : '');
                option.selected = true;
                field.add(option);
            }
            else
            {
                for (var i = 1; i <= optionMonths; i++)
                {
                    var option = document.createElement('option');
                    option.val = i;
                    option.text = i + ' Month' + (i != 1 ? 's' : '');
                    option.selected = (current == i);
                    field.add(option);
                }
            }
        }
    }
}

function CheckVariableDisplay()
{
    if (document.getElementsByClassName(trigger_using_variable_display_seg).length < 1) { return; }

    var segmentName = document.getElementsByClassName(label_segment)[0].innerText;

    var marker = `${marker_variable_display}_${segmentName}`;

    var hasMatch = document.getElementsByClassName(marker).length > 0;

    var variableDisplays = document.querySelectorAll(`.${marker_variable_display}`);

    if (variableDisplays.length > 0)
    {
        if (hasMatch) {
            variableDisplays.forEach(div => {
                div.style.display = div.classList.contains(marker) ? 'inherit' : 'none';
            });
        }
        else {
            variableDisplays.forEach(div => {
                div.style.display = div.classList.contains(marker_variable_display_default) ? 'inherit' : 'none';
            });
        }
    }
}

function CalculateAnualAmountRG(amountField, frequencyField)
{
    if (amountField == null) { amountField = document.getElementsByClassName(field_amount)[0]; }
    if (frequencyField == null) { frequencyField = document.getElementsByClassName(field_payment_frequency_new)[0]; }

    var amount = parseFloat(amountField.value);
    var multiplier = 0;

    if (amount != null && frequencyField.value != 'none') {
        switch (frequencyField.value) {
            case '4 Weekly': multiplier = 13.0446428571428; break;
            case 'Annual': multiplier = 1; break;
            case 'Annually': multiplier = 1; break;
            case 'Anually': multiplier = 1; break;
            case 'Bi Monthly': multiplier = 24; break;
            case 'Biannual': multiplier = 2; break;
            case 'Bi-Annual': multiplier = 2; break;
            case 'Bi-annually': multiplier = 2; break;
            case 'Bimonthly': multiplier = 24; break;
            case 'Bi-Monthly': multiplier = 24; break;
            case 'Biweekly': multiplier = 26.0892857142857; break;
            case 'Fortnightly': multiplier = 26.0892857142857; break;
            case 'Four Weekly': multiplier = 13.0446428571428; break;
            case 'Four- Weekly': multiplier = 13.0446428571428; break;
            case 'Four-Weekly': multiplier = 13.0446428571428; break;
            case 'Half Monthly': multiplier = 24; break;
            case 'Half Yearly': multiplier = 2; break;
            case 'Half-yearly': multiplier = 2; break;
            case 'Month': multiplier = 12; break;
            case 'Monthly': multiplier = 12; break;
            case 'Quarterley': multiplier = 4; break;
            case 'Quarterly': multiplier = 4; break;
            case 'Semi Annual': multiplier = 2; break;
            case 'Semi Annually': multiplier = 2; break;
            case 'Semi-Annual': multiplier = 2; break;
            case 'Semi-Annually': multiplier = 2; break;
            case 'Semi-Monthly': multiplier = 24; break;
            case 'Tri-Yearly': multiplier = 3; break;
            case 'Twice Yearly': multiplier = 2; break;
            case 'Weekly': multiplier = 52.1785714285714; break;
            case 'Yearly': multiplier = 1; break;
            case 'Every 4 weeks': multiplier = 13.0446428571428; break;
            default: multiplier = 0; break;
        }
    }
    else {
        amount = 0;
    }

    document.querySelectorAll(sys_auto_update_path + 'annual_amount').forEach(p => {
        p.innerText = (amount * multiplier).toFixed(2);
    });
}

function PopulateDonorTitle()
{
    var label = document.getElementsByClassName(label_donor_title)[0].innerText;
    var field = document.getElementsByClassName(field_donor_title)[0];
    for (var i = 0; i < field.options.length; i++)
    {
        if (field.options[i].value == label) {
            field.options[i].selected = true;
        }
    }
}